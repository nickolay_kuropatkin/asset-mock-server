package routes

import (
	"net/http"
	"encoding/json"
	"fmt"
)

func AssertDataHandler(w http.ResponseWriter, r *http.Request) {

	partners := Partners{
		PartnerIDs:[]string{"1","2"},
	}

	json, _ := json.Marshal(partners)
	fmt.Println("JSON:==>",json)

	w.WriteHeader(http.StatusOK)
	w.Write(json)
}