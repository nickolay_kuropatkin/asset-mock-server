package routes

import (
	"net/http"
	"encoding/json"
	"io/ioutil"
	"fmt"
)

type Partners struct {
	PartnerIDs []string `json:"partnerIDs"`
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Error reading request body", http.StatusInternalServerError)
	}

	req := Partners{}

	if body != nil {

		json.Unmarshal([]byte(body), &req)

	}

	fmt.Println(req)
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}