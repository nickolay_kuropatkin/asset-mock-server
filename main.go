package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"log"
	"bitbucket.org/nickolay_kuropatkin/asset-mock-server/src/routes"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/asset/v1/migrate", routes.IndexHandler).Methods("POST")
	router.HandleFunc("/asset/v1/assert/partners", routes.AssertDataHandler).Methods("GET")

	server := &http.Server{
		Addr:    "0.0.0.0:8084",
		Handler: router,
	}

	log.Fatal(server.ListenAndServe())
}